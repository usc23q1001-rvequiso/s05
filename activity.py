from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass

	def make_sound(self):
		pass

class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	# Setters
	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed

	def set_age(self, age):
		self._age = age

	# Getters
	def get_name(self):
		print(f'Cat Name: {self._name}')

	def get_breed(self):
		print(f'Cat Breed: {self._breed}')

	def get_age(self):
		print(f'Cat Age: {self._age}')

	# Abstact Methods
	def eat(self, food):
		print(f'Eaten {food}')

	def make_sound(self):
		print(f'MEEEEow')

	def call(self):
		print(f'{self._name} come here!')


class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	# Setters
	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed

	def set_age(self, age):
		self._age = age

	# Getters
	def get_name(self):
		print(f'Dog Name: {self._name}')

	def get_breed(self):
		print(f'Dog Breed: {self._breed}')

	def get_age(self):
		print(f'Dog Age: {self._age}')

	# Abstact Methods
	def eat(self, food):
		print(f'Eaten {food}')

	def make_sound(self):
		print(f'ARF ARF! AWOOOOOOOO')

	def call(self):
		print(f'{self._name} come here!')

# Test Cases

dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()